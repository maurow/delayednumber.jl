# Implements a number whose evaluation is delayed until it is
# specifically evaluated.
#
# References on discussions on delayed evaluation:
# https://groups.google.com/forum/#!topic/julia-dev/2aqKe9Q9dEc/discussion
# Delayed matrix:
# https://github.com/kk49/julia-delayed-matrix
# newer:
# https://groups.google.com/forum/#!searchin/julia-dev/delayed$20evaluation/julia-dev/PVs8Q-DTlts/GUKXLGZUzywJ
#

# MIT-licensed:
#
# Copyright (c) 2013 Mauro Werder
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

module DE
importall Base
#export DeNum, evalDe

immutable DeNum <: Number
    expr::Union(Expr,Symbol)
end
DeNum() = DeNum(:SYM)  # use symbol :SYM as default

### Unary operators
for op = (:sqrt, :exp, :sin, :cos, :tan)
    @eval ($op)(de::DeNum) = DeNum( Expr(:call, $op, de.expr) )
end

### Binary operators
# Some special definitions to stop ambiguity warnings:
/(de::DeNum, c::Complex)      = DeNum(:($(de.expr) / $(c)))
^(de::DeNum,c::Rational)      = DeNum(:($(de.expr) ^ $(c)))
^(de::DeNum,c::Integer)       = DeNum(:($(de.expr) ^ $(c)))
^(c::MathConst{:e},de::DeNum) = DeNum(:($(de.expr) ^ $(c)))
for op = (:+, :-, :*, :/, :^) 
    @eval ($op)(de1::DeNum, de2::DeNum) = DeNum( Expr(:call, $op, de1.expr, de2.expr) )
    @eval ($op)(de::DeNum, c::Number)   = DeNum( Expr(:call, $op, de.expr, c) )
    @eval ($op)(c::Number, de::DeNum)   = DeNum( Expr(:call, $op, c, de.expr) )
    ## this does not work because the second $-interpolation doesn't work:
    # @eval ($op)(de1::DeNum, de2::DeNum) = DeNum( :(($op)($(de1.expr), $(de2.expr))) )
    # @eval begin ( ($op)(c::Number, de::DeNum) = DeNum( :(($op)($c, $(de.expr))) ) ) end
    # @eval ($op)(de::DeNum, c::Number) = DeNum( :(($op)($(de.expr), $c)) )
end

### Ternary operators
for op = ()
    nothing
end

#### Evaluation functions (a bit hacky because of globals)
function evalDe(de::DeNum, val)  
    # This assumes used symbol is SYM
    global SYM = val  # this only works when SYM is in the global scope
    eval(de.expr)
end

function evalDe(de::DeNum, symbols, values) 
    # For any collection of symbols and corresponding values
    for (sym, val) in zip(symbols, values)
        eval(:(global $sym = $val))
    end
    out = eval(de.expr)
    # Set symbols contents to 'nothing' to catch some errors when this
    # function is called with wrong symbols:
    for sym in symbols
        eval(:($sym = nothing))
    end
    return out
end

end # module DE

############################
using DE

## examples
de = DE.DeNum()  # uses :SYM
de2 = (de + 5)*7*9
DE.evalDe(de2, 5)

de3 = DE.DeNum(:S2)
de4 = de2 + de3
DE.evalDe(de4, [:SYM, :S2], [3, 9])

## some tests:
de5 = DE.DeNum(:C3)
C3 = 5
eval(de5.expr)
DE.evalDe(de5, [:C3], [6])
@assert C3==5

fns = [x -> (5+x)/7.3 + x* 90
       x -> sqrt(5+x)
       x -> exp(sqrt(5+x))
       x -> 4//3 + x^3
       x -> (7.3x/(4 + 4im))^(3. - .7im)]
for (i,fn) in enumerate(fns)
    de = DE.DeNum()  # uses :SYM
    for x in {4, 4.5, (-4 + 3im)}
        #@show i, fn(x), DE.evalDe(fn(de), x),  fn(x)==DE.evalDe(fn(de), x)
        @assert fn(x)==DE.evalDe(fn(de), x)
    end
    de1 = DE.DeNum(:S1)
    de2 = DE.DeNum(:S2)
    for (x,y) in {(4,7.), (-4.5,5), ((-4 + 3im), 7//5)}
        @assert fn(x+y)==DE.evalDe(fn(de1+de2), [:S1, :S2], [x, y])
    end
end

## timings
n = int(1e3)
vals = linspace(0,9, n)

function g1(vals)
    out = similar(vals)
    for (i,x) in enumerate(vals)
        out[i] = fns[1](x)
    end
    return out
end

function g2(vals)
    out = similar(vals)
    de = fns[1](DE.DeNum())
    for (i,x) in enumerate(vals)
        out[i] = DE.evalDe(de, x)
    end
    return out
end

println("Doing math without DeNum:")
@time out1 = g1(vals);
println("Doing math with DeNum:")
@time out2 = g2(vals);
@assert all(out1.==out2)