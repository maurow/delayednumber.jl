# Implements a number whose evaluation is delayed until it is
# specifically evaluated.
#
# References on discussions on delayed evaluation:
# https://groups.google.com/forum/#!topic/julia-dev/2aqKe9Q9dEc/discussion
# Delayed matrix:
# https://github.com/kk49/julia-delayed-matrix
# newer:
# https://groups.google.com/forum/#!searchin/julia-dev/delayed$20evaluation/julia-dev/PVs8Q-DTlts/GUKXLGZUzywJ
#

# MIT-licensed:
#
# Copyright (c) 2013 Mauro Werder
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

module DE
importall Base
export DeAny, evalDe

immutable DeAny
    expr::Union(Expr,Symbol)
end
DeAny() = DeAny(:SYM)  # use symbol :SYM as default

# From discussion:
# https://groups.google.com/forum/#!topic/julia-users/X2-FoIXfQqs
zero(::Type{DeAny}) = 0
!=(::DeAny, ::Number) = true
zeros(::Type{DeAny}, dims...) = map(x->+(DeAny(),x),zeros(dims...))

# https://groups.google.com/d/msg/julia-dev/he7sO8Lyvm4/2i-Dx746KKAJ
# get all methods
function basefunctions()
    out = Symbol[]
    for nm in names(Base) 
        if isa(eval(nm), Function)
            push!(out, nm)
        end 
    end 
    return out
end 


### Unary operators
for op = (:sqrt, :exp, :sin, :cos, :tan, :length, :isempty, :endof, :!)
    @eval ($op)(de::DeAny) = DeAny( Expr(:call, $op, de.expr) )
end

### Binary operators
# Some special definitions to stop ambiguity warnings:
/(de::DeAny, c::Complex)      = DeAny(:($(de.expr) / $(c)))
^(de::DeAny,c::Rational)      = DeAny(:($(de.expr) ^ $(c)))
^(de::DeAny,c::Integer)       = DeAny(:($(de.expr) ^ $(c)))
^(c::MathConst{:e},de::DeAny) = DeAny(:($(de.expr) ^ $(c)))
for op = (:+, :-, :*, :/, :^, :(==))#, :push!, :map!, :size)
    @eval ($op)(de1::DeAny, de2::DeAny) = DeAny( Expr(:call, $op, de1.expr, de2.expr) )
    @eval ($op)(de::DeAny, c)   = DeAny( Expr(:call, $op, de.expr, c) )
    @eval ($op)(c, de::DeAny)   = DeAny( Expr(:call, $op, c, de.expr) )
    ## this does not work because the second $-interpolation doesn't work:
    # @eval ($op)(de1::DeAny, de2::DeAny) = DeAny( :(($op)($(de1.expr), $(de2.expr))) )
    # @eval ($op)(de1::DeAny, de2::DeAny) = DeAny( :(($op)($(de1.expr), $(de2.expr))) )
    # @eval begin ( ($op)(c::Number, de::DeAny) = DeAny( :(($op)($c, $(de.expr))) ) ) end
#    @eval ($op)(de::DeAny, c::Number) = DeAny( :(($($op))($(de.expr), $c)) )
end

### Ternary operators
for op = ()
    nothing
end

push!(de::DeAny, c)      = DeAny(:(push!($(de.expr), $(c))))

#### Evaluation functions (a bit hacky because of globals)
function evalDe(de::DeAny, val)  
    # This assumes used symbol is SYM
    global SYM = val  # this only works when SYM is in the global scope
    eval(de.expr)
end

function evalDe(de::DeAny, symbols, values) 
    # For any collection of symbols and corresponding values
    for (sym, val) in zip(symbols, values)
        eval(:(global $sym = $val))
    end
    out = eval(de.expr)
    # Set symbols contents to 'nothing' to catch some errors when this
    # function is called with wrong symbols:
    for sym in symbols
        eval(:($sym = nothing))
    end
    return out
end

end # module DE

############################
using DE

## examples
de = DE.DeAny()  # uses :SYM
de2 = (de + 5)*7*9
DE.evalDe(de2, 5)

de3 = DE.DeAny(:S2)
de4 = de2 + de3
DE.evalDe(de4, [:SYM, :S2], [3, 9])

## some tests:
de5 = DE.DeAny(:C3)
C3 = 5
eval(de5.expr)
DE.evalDe(de5, [:C3], [6])
@assert C3==5

fns = [x -> (5+x)/7.3 + x* 90
       x -> sqrt(5+x)
       x -> exp(sqrt(5+x))
       x -> 4//3 + x^3
       x -> (7.3x/(4 + 4im))^(3. - .7im)]
#       x -> x==3 ? 2 : x^3]
for (i,fn) in enumerate(fns)
    de = DE.DeAny()  # uses :SYM
    for x in {4, 4.5, (-4 + 3im)}
        #@show i, fn(x), DE.evalDe(fn(de), x),  fn(x)==DE.evalDe(fn(de), x)
        @assert fn(x)==DE.evalDe(fn(de), x)
    end
    de1 = DE.DeAny(:S1)
    de2 = DE.DeAny(:S2)
    for (x,y) in {(4,7.), (-4.5,5), ((-4 + 3im), 7//5)}
        @assert fn(x+y)==DE.evalDe(fn(de1+de2), [:S1, :S2], [x, y])
    end
end

## timings
n = int(1e3)
vals = linspace(0,9, n)

function g1(vals)
    out = similar(vals)
    for (i,x) in enumerate(vals)
        out[i] = fns[1](x)
    end
    return out
end

function g2(vals)
    out = similar(vals)
    de = fns[1](DE.DeAny())
    for (i,x) in enumerate(vals)
        out[i] = DE.evalDe(de, x)
    end
    return out
end

println("Doing math without DeAny:")
@time out1 = g1(vals);
println("Doing math with DeAny:")
@time out2 = g2(vals);
@assert all(out1.==out2)


# From discussion:
# https://groups.google.com/forum/#!topic/julia-users/X2-FoIXfQqs
daa = [de+3, de-5]
a=sparse(1:2, 1:2, daa)
try
    a*a
catch
    println("sparse multiply fails")
end
# works for normal array:
aa = zeros(DeAny, 5,5)
map(x->evalDe(x, 5), aa*aa)
